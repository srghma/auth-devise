const path = require('path')
const _merge = require('lodash/merge')

let defaultOptions = {
  paths: {
    signIn:          'auth/sign_in',
    signOut:         'auth/sign_out',
    validateToken:   'auth/validate_token',
    deleteAccount:   'auth',
    registerAccount: 'auth',
    updatePassword:  'auth',
    resetPassword:   'auth/password'
  },

  storageOptions: { expires: 7, secure: true }
}

module.exports = function nuxtAuthDevise (moduleOptions) {
  const options = _merge(defaultOptions, this.options.auth, moduleOptions)

  this.addVendor(['cookie', 'js-cookie'])

  this.addPlugin({
    src: path.join(__dirname, 'plugin.js'),
    options: options
  })
}

module.exports.meta = require('../package.json')
