import Cookies from 'js-cookie'
import Cookie from 'cookie'
import _pick from 'lodash/pick'
import _get from 'lodash/get'
import _forEach from 'lodash/forEach'
import _isEmpty from 'lodash/isEmpty'

const options = <%= serialize(options) %> // eslint-disable-line

// ----------------------------------------
// Utils
// ----------------------------------------
const authHeaderNames = ['access-token', 'client', 'expiry', 'token-type', 'uid']

class CookieStorage {
  constructor(ctx) {
    this.ctx = ctx
  }

  save (authHeaders) {
    if (this.ctx.isServer) return

    _isEmpty(authHeaders) ?
      authHeaderNames.forEach(h => Cookies.remove(h, options.storageOptions)) :
      authHeaderNames.forEach(h => Cookies.set(h, authHeaders[h], options.storageOptions))
  }
  load () {
    const cookieStr = this.ctx.isBrowser ? document.cookie : this.ctx.req.headers.cookie
    const cookies = Cookie.parse(cookieStr || '') || {}
    return _pick(authHeaders, cookies)
  }
}

const setAuthHeadersFromResponse = store => response => {
  const headers     = _get('headers', response)
  const authHeaders = _pick(authHeaderNames, headers)
  store.dispatch('setAuthHeaders', authHeaders)
}

function axiosSetHeaders (axios, headers) {
  forEach(headers, (k, v) => axios.setHeader(k, v))
}

// ----------------------------------------
// Store
// ----------------------------------------
function createAuthModule(ctx) {
  const cookieStorage = new CookieStorage(ctx)

  return {
    state: {
      user: null,
      authHeaders: null
    },

    getters: {
      signedIn: state => !!state.user
    },

    mutations: {
      setUser (state, user) {
        state.user = user
      },
      setAuthHeaders (state, authHeaders) {
        state.authHeaders = authHeaders
      }
    },

    actions: {
      setAuthHeaders (ctx, authHeaders) {
        ctx.commit('setAuthHeaders', authHeaders)
        cookieStorage.save(authHeaders)
        axiosSetHeaders(this.$axios, authHeaders)
      }

      async signIn (ctx, fields) {
        const res = await this.$axios.post(options.paths.signIn, fields)
        const user = res.data.data
        ctx.commit('setUser', user)
        return user
      },

      async signOut (ctx) {
        try {
          await this.$axios.delete(options.paths.signOut)
        } finally {
          <`3`>ctx.commit('setUser', null)
        }
      }
    }
  }
}

// ----------------------------------------
// Initialize
// ----------------------------------------
export default (ctx) => {
  const { store } = ctx
  // register vuex module
  store.registerModule('auth', createAuthModule(ctx))

  // from now each response will undate authHeaders in axios, store and cookies
  const setter = setAuthHeadersFromResponse(store)
  store.$axios.interceptors.response.use(resp => {
    setter(resp)
    return resp
  }, err => {
    setter(err.response)
    return Promise.reject(err)
  })
}
